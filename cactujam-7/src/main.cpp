#include <vector>
#include <map>
#include <string>
#include <filesystem>
#include <sstream>
#include <iostream>
#include <random>
#include <fstream>

#include <nlohmann/json.hpp>

#include <SFML/Graphics.hpp>


#define SAVEFILE_NAME "save.json"
#define TITLE "Spider Evolution"
#define MAX_STAGE 12
#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 800
#define SCREEN_WIDTH_F 800.0f
#define SCREEN_HEIGHT_F 800.0f
#define SPAWN_TIME_MIN 3000
#define SPAWN_TIME_MAX 5000
#define MOVE_TIME_MIN 1000
#define MOVE_TIME_MAX 5000
#define MOVE_DISTANCE_MIN -50.0f
#define MOVE_DISTANCE_MAX 50.0f
#define BOX_DAMAGE_TO_OPEN 3
#define MAX_BOXES 10
#define MAX_SPIDERS 50
#define TEXTURE_SCALE 0.25f



namespace fs = std::filesystem;



std::string filenameNoExtension(fs::path path) {
    return path.filename().replace_extension().string();
}



class AssetManager {

    public:
        static void addTextures(const fs::path);
        static sf::Texture &getTexture(const std::string);

    private:
        static std::map<std::string, sf::Texture> textures; 

};

std::map<std::string, sf::Texture> AssetManager::textures;

void AssetManager::addTextures(const fs::path path) {
    if (fs::is_directory(path)) {
        for (auto file: fs::directory_iterator(path)) {
            textures[filenameNoExtension(file.path().string())].loadFromFile(file.path().string());
        }
    } else {
        textures[filenameNoExtension(path.string())].loadFromFile(path.string());
    }
}

sf::Texture &AssetManager::getTexture(const std::string textureId) {
    auto search = textures.find(textureId);
    if (search != textures.end()) {
        return search->second;
    } else {
        throw;
    }
}


class Spider {

    public:
        Spider(int, double, double);
        bool operator ==(Spider r);
        bool operator !=(Spider r);

        void evolve();
        int earnMoney();

        int stage;
        bool holdState;
        bool evolveState;
        int moveTime;
        sf::Clock moveClock;
        sf::Sprite sprite;
    
    private:
        void setupSprite();

        int id;

        static int nextId;

};

int Spider::nextId = 0;

Spider::Spider(int stage = 1, double x = 0.0f, double y = 0.0f) {
    id = nextId;
    ++nextId;

    this->stage = stage;

    holdState = 0;
    evolveState = 0;

    moveTime = 0;
    
    setupSprite();
    sprite.setPosition(x, y);
}

bool Spider::operator==(Spider r) {
    return id == r.id;
}

bool Spider::operator!=(Spider r) {
    return id != r.id;
}

void Spider::evolve() {
    ++stage;
    setupSprite();
}

int Spider::earnMoney() {
    return stage * stage;
}

void Spider::setupSprite() {
    sf::Vector2f position = sprite.getPosition();
    sprite = sf::Sprite();

    std::stringstream ss;
    ss << "spider_stage_" << stage;
    sprite.setTexture(AssetManager::getTexture(ss.str()));

    sprite.setPosition(position);
    sprite.setScale(sf::Vector2f(TEXTURE_SCALE, TEXTURE_SCALE));
}


class Box {

    public:
        Box(double, double);
        bool operator ==(Box r);

        void hit();

        int damage;

        sf::Sprite sprite;
    
    private:
        int id;

        static int nextId;

        void setTexture();
};

int Box::nextId = 0;

Box::Box(double x = 0.0f, double y = 0.0f) {
    id = nextId;
    ++nextId;

    damage = 0;

    setTexture();
    sprite.setPosition(x, y);
    sprite.setScale(sf::Vector2f(TEXTURE_SCALE, TEXTURE_SCALE));
}

bool Box::operator==(Box r) {
    return id == r.id;
}

void Box::hit() {
    ++damage;

    if (damage < BOX_DAMAGE_TO_OPEN) {
        setTexture();
    }
}

void Box::setTexture() {
    std::stringstream ss;
    ss << "box_damage_" << damage;
    sprite.setTexture(AssetManager::getTexture(ss.str()));
}



void save(std::vector<Spider> spiders, std::vector<Box> boxes, int money) {
    nlohmann::json saveJson;

    for (Spider spider: spiders) {
        sf::Vector2f position = spider.sprite.getPosition();
        saveJson["spiders"].push_back({ {"stage", spider.stage}, {"x", position.x}, {"y", position.y} });
    }

    for (Box box: boxes) {
        sf::Vector2f position = box.sprite.getPosition();
        saveJson["boxes"].push_back({ {"x", position.x}, {"y", position.y} });
    }

    saveJson["money"] = money;

    std::ofstream saveFile(SAVEFILE_NAME);
    if (saveFile.bad()) {
        throw;
    }
    saveFile << saveJson;
    saveFile.close();
}

void load(std::vector<Spider> &spiders, std::vector<Box> &boxes, int &money) {
    if (!fs::exists(SAVEFILE_NAME)) {
        return;
    }

    std::ifstream saveFile(SAVEFILE_NAME);
    if (saveFile.bad()) {
        throw;
    }
    nlohmann::json saveJson;
    saveFile >> saveJson;
    saveFile.close();

    spiders.clear();
    for (nlohmann::json spider: saveJson["spiders"]) {
        spiders.push_back(Spider(spider["stage"], spider["x"], spider["y"]));
    }

    boxes.clear();
    for (nlohmann::json box: saveJson["boxes"]) {
        boxes.push_back(Box(box["x"], box["y"]));
    }

    money = saveJson["money"];
}

int main() {
    std::random_device randomDevice;
    std::mt19937 generator(randomDevice());

    std::uniform_real_distribution randomX(0.0f, SCREEN_WIDTH_F - 50.0f);
    std::uniform_real_distribution randomY(0.0f, SCREEN_HEIGHT_F - 50.0f);
    std::uniform_int_distribution randomSpawnTime(SPAWN_TIME_MIN, SPAWN_TIME_MAX);
    std::uniform_int_distribution randomMoveTime(MOVE_TIME_MIN, MOVE_TIME_MAX);
    std::uniform_real_distribution randomMoveDistance(MOVE_DISTANCE_MIN, MOVE_DISTANCE_MAX);

    AssetManager::addTextures("assets/textures");

    sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), TITLE, sf::Style::Titlebar | sf::Style::Close);
    window.setVerticalSyncEnabled(1);

    // TODO: Move that into AssetManager (AssetManager).
    sf::Font robotoFont;
    if (!robotoFont.loadFromFile("assets/fonts/roboto.ttf")) {
        throw;
    }

    sf::Text moneyText;
    moneyText.setFont(robotoFont);
    moneyText.setFillColor(sf::Color::Black);
    moneyText.setCharacterSize(24);

    sf::Sprite background;
    background.setTexture(AssetManager::getTexture("background"));

    std::vector<Spider> spiders;
    std::vector<Box> boxes;

    int money = 0;

    for (int i = 0; i < 2; ++i) {
        boxes.push_back(Box(randomX(generator), randomY(generator)));
    }

    load(spiders, boxes, money);

    sf::Clock spawnClock;
    int spawnTime = randomSpawnTime(randomDevice);

    while (window.isOpen()) {
        // Input
        sf::Vector2f cursor = window.mapPixelToCoords(sf::Mouse::getPosition(window));

        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                save(spiders, boxes, money);

                window.close();
            } else if (event.type == sf::Event::MouseButtonPressed) {
                // TODO: Some property class for holding stuff?
                for (Spider &spider: spiders) {
                    sf::FloatRect spiderBounds = spider.sprite.getGlobalBounds();

                    if (spiderBounds.contains(cursor)) {
                        money += spider.earnMoney();
                        spider.holdState = 1;
                        break;
                    }
                }

                for (Box &box: boxes) {
                    sf::FloatRect boxBounds = box.sprite.getGlobalBounds();

                    if (boxBounds.contains(cursor)) {
                        box.hit();
                        // TODO: Move this to the Box class? It would make sense...
                        if (box.damage >= BOX_DAMAGE_TO_OPEN) {
                            sf::Vector2f position = box.sprite.getPosition();
                            spiders.push_back(Spider(1, position.x, position.y));
                            boxes.erase(std::find(boxes.begin(), boxes.end(), box));
                        }

                        break;
                    }
                }
            } else if (event.type == sf::Event::MouseButtonReleased) {
                for (Spider &spider: spiders) {
                    if (spider.holdState) {
                        spider.holdState = 0;
                        spider.evolveState = 1;
                    }
                }
            } else if (event.type == sf::Event::KeyPressed) {
                switch (event.key.code) {
                    #ifdef DEBUG
                    case sf::Keyboard::S:
                        spiders.push_back(Spider(1, randomX(generator), randomY(generator)));
                        break;
                    
                    case sf::Keyboard::C:
                        spiders.clear();
                        boxes.clear();
                        break;
                    
                    case sf::Keyboard::B:
                        boxes.push_back(Box(randomX(generator), randomY(generator)));
                        break;
                    #endif
                }
            }
        }


        // Tick

        // TODO: "Held spider" and "Evolving spider" pointers/references would make more sense, as only one spider can be held, or evolving, at one time. Also saves preformance.
        for (Spider &spider: spiders) {
            if (spider.holdState) {
                // Move
                sf::FloatRect spiderBounds = spider.sprite.getGlobalBounds();
                spider.sprite.setPosition(cursor.x - spiderBounds.width / 2, cursor.y - spiderBounds.height / 2);

                break;
            } else if (spider.evolveState) {
                sf::FloatRect spiderBounds = spider.sprite.getGlobalBounds();
                for (Spider &spider2: spiders) {
                    sf::FloatRect spider2Bounds = spider2.sprite.getGlobalBounds();                        
                    if (spider != spider2 && spiderBounds.intersects(spider2Bounds) && spider.stage == spider2.stage && spider.stage < MAX_STAGE) {
                        // Evolve
                        spider.evolve();
                        spiders.erase(std::find(spiders.begin(), spiders.end(), spider2));

                        break;
                    }
                }

                spider.evolveState = 0;
                break;
            } else if (spider.moveClock.getElapsedTime() > sf::milliseconds(spider.moveTime)) {
                spider.moveTime = randomMoveTime(generator);
                spider.sprite.move(sf::Vector2f(randomMoveDistance(generator), randomMoveDistance(generator)));
                money += spider.earnMoney();
                spider.moveClock.restart();
            }

            sf::Vector2f position = spider.sprite.getPosition();
            // TODO: It shouldn't be constant
            if (position.x > SCREEN_WIDTH_F - 50.0f) {
                position.x = SCREEN_WIDTH_F - 50.0f;
            } else if (position.x < 0) {
                position.x = 0;
            }
            if (position.y > SCREEN_HEIGHT_F - 50.0f) {
                position.y = SCREEN_HEIGHT_F - 50.0f;
            } else if (position.y < 0) {
                position.y = 0;
            }
            spider.sprite.setPosition(position);
        }

        for (Box &box: boxes) {
            sf::Vector2f position = box.sprite.getPosition();
            // TODO: It shouldn't be constant
            if (position.x > SCREEN_WIDTH_F - 50.0f) {
                position.x = SCREEN_WIDTH_F - 50.0f;
            } else if (position.x < 0) {
                position.x = 0;
            }
            if (position.y > SCREEN_HEIGHT_F - 50.0f) {
                position.y = SCREEN_HEIGHT_F - 50.0f;
            } else if (position.y < 0) {
                position.y = 0;
            }
            box.sprite.setPosition(position);
        }

        // Spawn new box
        sf::Time fromLastSpawn = spawnClock.getElapsedTime();
        if (spiders.size() + boxes.size() <= MAX_SPIDERS && boxes.size() <= MAX_BOXES && fromLastSpawn > sf::milliseconds(spawnTime)) {
            boxes.push_back(Box(randomX(generator), randomY(generator)));
            save(spiders, boxes, money);
            spawnTime = randomSpawnTime(randomDevice);
            spawnClock.restart();
        }

        std::stringstream ss;
        ss << "Money: " << money;
        moneyText.setString(ss.str());


        // Draw

        window.clear(sf::Color::White);
        window.draw(background);

        // TODO: ...and also a property class for drawing stuff
        for (auto itr = spiders.end() - 1; itr != spiders.begin() - 1; --itr) {
            window.draw(itr->sprite);
        }

        for (auto itr = boxes.end() - 1; itr != boxes.begin() - 1; --itr) {
            window.draw(itr->sprite);
        }

        window.draw(moneyText);

        window.display();
    }
}