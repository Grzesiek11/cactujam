#include <ncurses.h>
#include <string>
#include <vector>
#include <cstdlib>
#include <locale>
#include <thread>
#include <mutex>

std::mutex m_writeToConsole;

int menu(std::vector<std::string> choices, WINDOW *win, bool &killFlag) {
    int choice = 0;
    int y = getcury(win);
    nodelay(win, 1);
    while (1) {
        m_writeToConsole.lock();
        wmove(win, y, 0);
        for (int i = 0; i < choices.size(); ++i) {
            if (i == choice) {
                wattron(win, A_STANDOUT);
            }
            wprintw(win, choices[i].c_str());
            if (i == choice) {
                wattroff(win, A_STANDOUT);
            }
            wprintw(win, "\n");
        }
        wrefresh(win);
        m_writeToConsole.unlock();
        int ch = 0;
        while (ch == ERR) {
            ch = wgetch(win);
            if (killFlag) {
                return -1;
            }
        }
        switch (ch) {
            case KEY_UP:
                --choice;
                break;
            case KEY_DOWN:
                ++choice;
                break;
            case '\n':
                return choice;
        }
        if (choice < 0) {
            choice = 0;
        } else if (choice > choices.size() - 1) {
            choice = choices.size() - 1;
        }
    }
    //return -1;
}

int random(int min, int max) {
    return rand() % max + min;
}

void game();

int main() {
    cbreak();
    srand(time(NULL));
    setlocale(LC_CTYPE,"C-UTF-8");
    std::vector<std::string> choices = {
        "Play",
        "Exit"
    };
    initscr();
    keypad(stdscr, 1);
    noecho();
    int choice = 0;
    while (1) {
        clear();
        printw("Sneaky Virus v1.0\nby Grzesiek11, licensed under GNU GPL v3.0\n");
        bool flag = 0;
        switch (menu(choices, stdscr, flag)) {
            case 0:
                game();
                break;
            case 1:
                return 0;
        }
    }
    endwin();
}

bool randomMinigame(WINDOW *gameWin, bool &isUp);
void timer(WINDOW *timerWin, int seconds, bool &isUp, bool &end);

void game() {
    const int maxHp = 5;
    int hp = 5;
    int level = 0;
    WINDOW *levelWin = newwin(1, 80 - maxHp, 0, 0);
    WINDOW *hpWin = newwin(1, maxHp, 0, 80 - maxHp);
    WINDOW *gameWin = newwin(22, 80, 1, 0);
    keypad(gameWin, 1);
    WINDOW *timerWin = newwin(1, 80, 23, 0);
    while (hp > 0) {
        wclear(hpWin);
        for (int i = 0; i < maxHp - hp; ++i) {
            waddch(hpWin, ' ');
        }
        for (int i = 0; i < hp; ++i) {
            waddch(hpWin, 'X');
        }
        wrefresh(hpWin);
        wclear(levelWin);
        wprintw(levelWin, "%i", level);
        wrefresh(levelWin);
        wclear(gameWin);
        bool isUp = 0;
        bool end = 0;
        std::thread timerThread(timer, timerWin, 10, std::ref(isUp), std::ref(end));
        if (randomMinigame(gameWin, isUp)) {
            ++level;
        } else {
            --hp;
        }
        end = 1;
        timerThread.join();
    }
}

bool randomMinigame(WINDOW *gameWin, bool &isUp) {
    switch(random(1, 1)) {
        case 1:
            wprintw(gameWin, "Czy chcesz umrzec?\n");
            std::vector<std::string> choices(5, "Tak");
            int good = random(0, 4);
            choices[good] = "Nie";
            return menu(choices, gameWin, isUp) == good;
    }
    return 0;
}

void timer(WINDOW *timerWin, int seconds, bool &isUp, bool &end) {
    const int timerEnd = time(NULL) + seconds;
    int currentOld = -1;
    while (!(isUp || end)) {
        int current = timerEnd - time(NULL);
        if (current != currentOld) {
            m_writeToConsole.lock();
            currentOld = current;
            wclear(timerWin);
            for (int i = 0; i < current; ++i) {
                for (int j = 0; j < 80 / seconds; ++j) {
                    if (i == current - 1 && j == 80 / seconds - 1) {
                        waddch(timerWin, '>');
                    } else {
                        waddch(timerWin, '=');
                    }
                }
            }
            if (current <= 0) {
                isUp = 1;
            }
            wrefresh(timerWin);
            m_writeToConsole.unlock();
        }
    }
}