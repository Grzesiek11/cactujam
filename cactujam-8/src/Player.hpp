#pragma once

#include "Entity.hpp"

#include <SFML/Graphics.hpp>

class Player : public Entity {

    public:
        enum class MoveDirection {
            Right,
            Left,
            None
        };

        virtual std::string entityType();

        Player();
        Player(bool*);

        const sf::Sprite getBagSprite();
        const double getLeakage();
        const int getLeftSand();

        #ifndef NDEBUG
        void setLeakage(double);
        #endif

        void move(const MoveDirection);
        void jump(std::vector<Entity*>);
        void attack();
        void punchBag(double);

        virtual void tick(std::vector<Entity*>);
    
    private:
        bool inAir(const sf::Sprite);

        bool *winFlag;

        MoveDirection moveDirection;
        int jumpTimer;
        int attackTimer;
        int moving;

        sf::Sprite bagSprite;
        sf::Clock grainDropTimer;
        int sand;
        double leakFraction;
    
};