#include "Ground.hpp"
#include "AssetManager.hpp"
#include "globals.hpp"

#include <iostream>

std::string Ground::entityType() {
    return "Ground";
}

Ground::Ground() {
    texture = AssetManager::getTexture("ground_tile");
    texture->setRepeated(true);

    sprite.setTexture(*texture);
    sprite.setTextureRect(sf::IntRect(0, 0, WINDOW_WIDTH + texture->getSize().y, texture->getSize().y));

}

void Ground::tick(std::vector<Entity*> entities) {
    sprite.setPosition(- findEntity("Player", entities)->getX() % (int) texture->getSize().x, WINDOW_HEIGHT + LOWER_GROUND - texture->getSize().y);
}