target_sources(game
    PRIVATE
        main.cpp
        globals.cpp

        AssetManager.cpp

        Entity.cpp
        Ground.cpp
        Background.cpp
        Player.cpp
        Monster.cpp
        Finish.cpp
)
