#include "AssetManager.hpp"

namespace fs = std::filesystem;

std::string filenameNoExtension(fs::path path) {
    return path.filename().replace_extension().string();
}

std::map<std::string, sf::Texture> AssetManager::textures;
std::map<std::string, sf::Font> AssetManager::fonts;
std::map<std::string, sf::SoundBuffer> AssetManager::sounds;

void AssetManager::addTextures(const fs::path path) {
    if (fs::is_directory(path)) {
        for (auto file: fs::directory_iterator(path)) {
            textures[filenameNoExtension(file.path().string())].loadFromFile(file.path().string());
        }
    } else {
        textures[filenameNoExtension(path.string())].loadFromFile(path.string());
    }
}

sf::Texture *AssetManager::getTexture(const std::string textureId) {
    auto search = textures.find(textureId);
    if (search != textures.end()) {
        return &search->second;
    } else {
        throw;
    }
}

void AssetManager::addFonts(const fs::path path) {
    if (fs::is_directory(path)) {
        for (auto file: fs::directory_iterator(path)) {
            fonts[filenameNoExtension(file.path().string())].loadFromFile(file.path().string());
        }
    } else {
        fonts[filenameNoExtension(path.string())].loadFromFile(path.string());
    }
}

sf::Font *AssetManager::getFonts(const std::string fontId) {
    auto search = fonts.find(fontId);
    if (search != fonts.end()) {
        return &search->second;
    } else {
        throw;
    }
}

void AssetManager::addSounds(const fs::path path) {
    if (fs::is_directory(path)) {
        for (auto file: fs::directory_iterator(path)) {
            sounds[filenameNoExtension(file.path().string())].loadFromFile(file.path().string());
        }
    } else {
        sounds[filenameNoExtension(path.string())].loadFromFile(path.string());
    }
}
sf::SoundBuffer *AssetManager::getSounds(const std::string soundId) {
    auto search = sounds.find(soundId);
    if (search != sounds.end()) {
        return &search->second;
    } else {
        throw;
    }
}