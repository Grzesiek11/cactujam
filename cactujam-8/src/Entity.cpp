#include "Entity.hpp"
#include "globals.hpp"

std::string Entity::entityType() {
    return "";
}

const int Entity::getX() {
    return x;
}

const sf::Sprite Entity::getSprite() {
    return sprite;
}

void Entity::updateRelativePosition(int playerX) {
    sprite.setPosition(x - playerX, sprite.getPosition().y);
}

void Entity::updateRelativePosition(std::vector<Entity*> entities) {
    updateRelativePosition(findEntity("Player", entities)->getX());
}