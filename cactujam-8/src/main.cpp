#include "AssetManager.hpp"
#include "Player.hpp"
#include "Monster.hpp"
#include "Ground.hpp"
#include "Background.hpp"
#include "Finish.hpp"
#include "globals.hpp"

#include <iostream>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

int main() {
    AssetManager::addTextures("res/textures");
    AssetManager::addFonts("res/fonts");
    AssetManager::addSounds("res/audio/sounds");

    sf::Music music;
    music.openFromFile("res/audio/music/level1.wav");
    music.setLoop(true);
    music.play();

    sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), NAME);
    window.setVerticalSyncEnabled(true);

    sf::Clock tickClock;
    sf::Clock gameClock;

    sf::Text text;
    text.setFont(*AssetManager::getFonts("quicksand"));
    text.setFillColor(sf::Color::Black);
    text.setPosition(10.0, 10.0);

    int frameTime = 0, tickTime = 0;

    std::vector<Entity*> entities;

    bool winFlag = false;
    Player player(&winFlag);

    Ground ground;
    Background background;

    entities.push_back(&player);
    entities.push_back(new Monster);
    entities.push_back(new Finish);
    entities.push_back(&ground);
    entities.push_back(&background);

    while (window.isOpen()) {
        sf::Clock frameTimeClock;

        // Event handling

        sf::Event event;
        while (window.pollEvent(event)) {
            switch (event.type) {
                
                case sf::Event::Closed:
                    window.close();
                    break;
                
                case sf::Event::KeyPressed:
                    switch (event.key.code) {
                        #ifndef NDEBUG
                        case sf::Keyboard::Equal:
                            player.setLeakage(player.getLeakage() + 0.01);
                            break;
                        
                        case sf::Keyboard::Subtract:
                            player.setLeakage(player.getLeakage() - 0.01);
                            break;
                        #endif
                        
                        case sf::Keyboard::Escape:
                            window.close();
                            break;
                    }
                    break;
                
            }
        }

        // Tick

        sf::Clock tickTimeClock;

        if (tickClock.getElapsedTime().asMilliseconds() >= 1000 / TPS) {

            if (!(sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && sf::Keyboard::isKeyPressed(sf::Keyboard::Left))) {
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
                    player.move(Player::MoveDirection::Right);
                } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && player.getX() > 0) {
                    player.move(Player::MoveDirection::Left);
                }
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z)) {
                player.jump(entities);
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::X)) {
                player.attack();
            }

            for (Entity *entity: entities) {
                entity->tick(entities);
            }
            
            std::stringstream ss;
            #ifndef NDEBUG
            ss << "MSPT: " << tickTime / 1000.0 << "\nMSPF: " << frameTime / 1000.0 << " (FPS: " << 1000000.0 / frameTime << ")\nX: " << player.getX() << " Y: " << player.getSprite().getPosition().y << '\n';
            #endif
            ss << "Sand: " << (double) player.getLeftSand() / STARTING_SAND * 100 << "%\nBag health: " << 100 - player.getLeakage() * 100 << "%\nTime: " << (int) gameClock.getElapsedTime().asSeconds();
            text.setString(ss.str());

            tickClock.restart();
        }

        tickTime = tickTimeClock.getElapsedTime().asMicroseconds();

        // Draw

        window.clear();

        window.draw(background.getSprite());

        for (Entity *entity: entities) {
            std::string type = entity->entityType();
            if (type != "Background" && type != "Player" && type != "Ground") {
                window.draw(entity->getSprite());
            }
        }

        window.draw(player.getSprite());
        window.draw(player.getBagSprite());
        window.draw(ground.getSprite());
        window.draw(text);

        window.display();

        frameTime = frameTimeClock.getElapsedTime().asMicroseconds();
    }
}