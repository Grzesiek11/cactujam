#pragma once

#include "Entity.hpp"
#include "Player.hpp"
#include "Ground.hpp"

class Background : public Entity {
    
    public:
        Background();

        virtual std::string entityType();

        virtual void tick(std::vector<Entity*>);
    
    private:
        sf::Texture *texture;
};