#include "Finish.hpp"
#include "AssetManager.hpp"
#include "Player.hpp"
#include "globals.hpp"

std::string Finish::entityType() {
    return "Finish";
}

Finish::Finish() {
    sprite.setTexture(*AssetManager::getTexture("finish"));
    x = 2000;
    sprite.setPosition(0.0, 145.0);
}

void Finish::tick(std::vector<Entity*> entities) {
    updateRelativePosition(entities);
}