#pragma once

#include "Entity.hpp"
#include "Player.hpp"

class Monster : public Entity {
    
    public:
        virtual std::string entityType();

        Monster();

        void damage(int);

        virtual void tick(std::vector<Entity*>);
    
    private:
        sf::Clock attackTimer;

        int hp;
    
};