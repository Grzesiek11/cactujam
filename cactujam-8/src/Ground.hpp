#pragma once

#include "Entity.hpp"
#include "Player.hpp"

class Ground : public Entity {
    
    public:
        virtual std::string entityType();
        
        Ground();

        virtual void tick(std::vector<Entity*>);
    
    private:
        sf::Texture *texture;
    
};