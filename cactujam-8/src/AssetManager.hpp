#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include <filesystem>
#include <map>


class AssetManager {

    public:
        static void addTextures(const std::filesystem::path);
        static sf::Texture *getTexture(const std::string);

        static void addFonts(const std::filesystem::path);
        static sf::Font *getFonts(const std::string);

        static void addSounds(const std::filesystem::path);
        static sf::SoundBuffer *getSounds(const std::string);

    private:
        static std::map<std::string, sf::Texture> textures;

        static std::map<std::string, sf::Font> fonts;

        static std::map<std::string, sf::SoundBuffer> sounds;

};