#pragma once

class Player;

#include <SFML/Graphics.hpp>

class Entity {

    public:
        virtual std::string entityType();

        const int getX();
        const sf::Sprite getSprite();

        virtual void tick(std::vector<Entity*>) = 0;
    
    protected:
        void updateRelativePosition(int);
        void updateRelativePosition(std::vector<Entity*>);

        sf::Sprite sprite;

        int x;
    
};