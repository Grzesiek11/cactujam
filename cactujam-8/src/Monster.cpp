#include "Monster.hpp"
#include "AssetManager.hpp"
#include "globals.hpp"
#include "Player.hpp"

#include <cmath>
#include <iostream>

std::string Monster::entityType() {
    return "Monster";
}

Monster::Monster() {
    x = 1000;
    hp = 5;
    sprite.setTexture(*AssetManager::getTexture("monster"));
    sprite.move(0.0, 300.0);
    sprite.setScale(0.5, 0.5);
}

void Monster::damage(int strenght) {
    hp -= strenght;
}

void Monster::tick(std::vector<Entity*> entities) {
    if (hp > 0) {
        Player *player = dynamic_cast<Player*>(findEntity("Player", entities));
        sf::Sprite bag = player->getBagSprite();
        if (abs(bag.getPosition().x - sprite.getPosition().x) < 500) {
            if (bag.getPosition().x / 2 > sprite.getPosition().x / 2) {
                x += 2;
            } else {
                x -= 2;
            }
            if (bag.getPosition().y / 2 > sprite.getPosition().y / 2) {
                sprite.move(0.0, 2.0);
            } else if (bag.getPosition().y / 2 < sprite.getPosition().y / 2) {
                sprite.move(0.0, -2.0);
            }
        }
        if (bag.getGlobalBounds().intersects(sprite.getGlobalBounds()) && attackTimer.getElapsedTime().asMilliseconds() > 1000) {
            player->punchBag(0.01);
            attackTimer.restart();
        }
        updateRelativePosition(entities);
    } else {
        sprite.setScale(0.0, 0.0);
    }
}