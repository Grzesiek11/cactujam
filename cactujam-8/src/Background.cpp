#include "Background.hpp"
#include "AssetManager.hpp"
#include "globals.hpp"

std::string Background::entityType() {
    return "Background";
}

Background::Background() {
    texture = AssetManager::getTexture("background");
    texture->setRepeated(true);
    
    sprite.setTexture(*texture);
    sprite.setTextureRect(sf::IntRect(0, 0, texture->getSize().x * 2, texture->getSize().y));
}

void Background::tick(std::vector<Entity*> entities) {
    sprite.setPosition(- findEntity("Player", entities)->getX() % (int) texture->getSize().x, - (int) findEntity("Ground", entities)->getSprite().getTexture()->getSize().y + LOWER_GROUND + LOWER_BACKGROUND);
}